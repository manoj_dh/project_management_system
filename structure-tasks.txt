# Project Manager Structure #

    # User :
        - user_id
        - user_email
        - user_name
        - Name & Last Name
        - Initials
        - Bio
        - user_image ( attachement_id )
        - Type : Administrator / Client / Tester / Developer 
        - is_confirm
        - is_activated
        - created_at

    # Projects
        - project_id
        - Name
        - Description
        - Project Logo / Image        
        - team_id
        - created_by ( user_id )
        - created_at

    # Issue_list
        - issue_id
        - Title
        - Description
        - assigned_to ( user_id )
        - attachement_ids
        - project_id
        - priority
        - due_date
        - created_by ( user_id )
        - created_at
        - modified_at
        - last_modified_by ( user_id )
        - status : Hold / Open / Working / Closed
        
    # Attachments 
        - attachement_id
        - Title
        - file
        - project_id
        - created_by ( user_id )
        - created_at

    # Team
        - team_id
        - project_id
        - team_members ( user_id )
        - team_leader ( user_id )
        - created_by ( user_id )
        - created_at
    
    # Comments
        - comment_id
        - Title
        - description
        - issue_id
        - project_id
        - created_by ( user_id )
        - created_at


# Task To Perform #

1. User Activity
    1.1 Create Login / Sign-Up Feature
    1.2 Forgot Password Feature
    1.3 Update Profile Feature
    1.4 Deactivate User Profile
    1.5 Delete User Profile

2. Project Activity
    2.1 Create / Edit / Delete Project ( Only Admin / Client )
        2.1.1 Project About
        2.1.2 Invite Team Member

    2.2 Create / Edit Issue to Project ( All can create )        
        2.2.1 Assign to Team Member         
        2.2.2 Manage Attachments
        2.2.2 Manage Activity / Comments

    2.3 Delete Issue from Project ()