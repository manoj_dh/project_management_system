import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SiteHeader from '../partials/Header';
import SiteFooter from '../partials/footer';

class HomePage extends Component {
    render() {
        return (
            <div className="main_wrapper">
                <SiteHeader/>
                <SiteFooter/>
            </div>
        );
    }
}

HomePage.propTypes = {

};

export default HomePage;